<?php

require 'vendor/autoload.php';

use SW\Enciclopedia;

$enciclopedia = new Enciclopedia();

$nombrePelicula = "The Phantom Menace";

$planetas = $enciclopedia->getPlanetasporPelicula($nombrePelicula);

echo "LOS PLANETAS QUE APARECEN EN LA PELICULA " . $nombrePelicula . " SON: " .PHP_EOL;

echo "<ul>";
foreach ($planetas as $planeta){
    echo "<li>" . $planeta->getNombre() . "</li>";
}
echo "</ul>";
