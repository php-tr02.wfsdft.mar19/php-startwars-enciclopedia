<?php

namespace SW;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Psr7;


/*class Planeta {
    private $nombre;
    private $poblacion;
    private $urlPlaneta;

    public function __construct($urlPlaneta) {
        $this->urlPlaneta = $urlPlaneta;
        //$this->poblacion = $poblacion;
    }

    public function get_planeta() {
        try {
        $client = new Client();
        $resPlaneta = $client->request('GET', $this->urlPlaneta);
        return json_decode($resPlaneta->getBody(), true)['name'];
        }
        catch (ClientException $e) {
            echo Psr7\str($e->getRequest());
            echo Psr7\str($e->getResponse());
        }
    }
}*/

class Planeta {
    private $nombre;
    private $poblacion;

    /**
     * Planeta constructor.
     * @param $nombre
     * @param $poblacion
     */
    public function __construct($nombre, $poblacion)
    {
        $this->nombre = $nombre;
        $this->poblacion = $poblacion;
    }

    public function getNombre()
    {
        return $this->nombre;
    }

    public function getPoblacion()
    {
        return $this->poblacion;
    }
}
