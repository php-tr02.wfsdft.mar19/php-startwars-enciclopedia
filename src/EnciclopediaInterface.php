<?php

namespace SW;

interface EnciclopediaInterface {
    public function getPlanetasporPelicula(String $nombrePelicula): array;
}
