<?php

namespace SW;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Psr7;

class Enciclopedia   {
    private $client;
    private $apiUrl = "https://swapi.co/api/";

    public function __construct()
    {
        $this->client = new Client();
    }

    public function getPlanetasporPelicula(String $nombrePelicula){
        // TODO: Implement getPlanetasporPelicula() method.
        try {
            $res = $this->client->request('GET', $this->apiUrl . 'films');
        }
        catch (ClientException $e) {
            echo Psr7\str($e->getRequest());
            echo Psr7\str($e->getResponse());
        }
        $planetasPelicula = [];
        $peliculas = json_decode($res->getBody(), true)['results'];
        foreach ($peliculas as $valor)
        {
            if ($valor['title'] == $nombrePelicula) {
                foreach ($valor['planets'] as $planetas){
                    $resPlaneta = $this->client->request('GET', $planetas);
                    $contenidoPlaneta = json_decode($resPlaneta->getBody(), true);
                    $planetasPelicula[] = new Planeta(
                        $contenidoPlaneta['name'],
                        $contenidoPlaneta['population']
                    );

                }
            }
        }
        return $planetasPelicula;
    }

}
